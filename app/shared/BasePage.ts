import {topmost} from "ui/frame";
import {Page} from "ui/page";
import {Observable, EventData} from "data/observable";
import {View} from "ui/core/view";

let appViewModel = new Observable({selectedPage: "home"});

export abstract class BasePage {

    abstract mainContentLoaded(args: EventData): void;
    
    loaded(args): void{
        let page = <Page>args.object;
        page.bindingContext = appViewModel;   
    }
    
    toggleDrawer(): void {
        let page = <Page>topmost().currentPage;
        let drawer = <any>page.getViewById("drawer");
        drawer.toggleDrawerState();
    }
    
    navigate(args): void{

        let pageName = args.view.id;

        console.log('BasePage.navigate ' + pageName);

        appViewModel.set("selectedPage", pageName);

        topmost().navigate("pages/" + pageName + "/" + pageName);
    }
}

