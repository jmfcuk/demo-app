import {BasePage} from "../../shared/BasePage";
import {Page} from "ui/page";
import {topmost} from "ui/frame";
import {Observable, EventData} from "data/observable";
import {View} from "ui/core/view";
import {ListView, ItemEventData} from "ui/list-view";
import {HomeViewModel} from './home-model';
import {UserDetailModel} from '../user-detail/user-detail-model';

let model = new HomeViewModel();

export class HomePage extends BasePage{

    private _model: HomeViewModel;

    constructor() {
        super();
    }

    mainContentLoaded(args: EventData): void {

        let view = <View>args.object;

        view.bindingContext = model; //new HomeViewModel();
    }


    // mainContentLoaded(args:EventData): void {

    //     let view = <View>args.object;

    //     view.bindingContext = this._model; //new HomeViewModel();
    // }

    searchSubmit(): void {
        //alert('searchSubmit');
    }

    listTap(args: ItemEventData): void {

        let udm: UserDetailModel = model.array.getItem(args.index);
    }
}
// export = new HomePage();
