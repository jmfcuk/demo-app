"use strict";
var BasePage_1 = require("../../shared/BasePage");
var home_model_1 = require('./home-model');
var model = new home_model_1.HomeViewModel();
var HomePage = (function (_super) {
    __extends(HomePage, _super);
    function HomePage() {
        _super.call(this);
    }
    HomePage.prototype.mainContentLoaded = function (args) {
        var view = args.object;
        view.bindingContext = model; //new HomeViewModel();
    };
    // mainContentLoaded(args:EventData): void {
    //     let view = <View>args.object;
    //     view.bindingContext = this._model; //new HomeViewModel();
    // }
    HomePage.prototype.searchSubmit = function () {
        //alert('searchSubmit');
    };
    HomePage.prototype.listTap = function (args) {
        var udm = model.array.getItem(args.index);
    };
    return HomePage;
}(BasePage_1.BasePage));
exports.HomePage = HomePage;
// export = new HomePage();
//# sourceMappingURL=home.js.map