import {Observable} from "data/observable";
import {ObservableArray}  from 'data/observable-array';
import {UserDetailModel} from '../user-detail/user-detail-model';

export class HomeViewModel extends Observable {

    busy: boolean = true;

    heading: string = 'Who\'s in the house?';

    private _array: ObservableArray<any>;
    
    get array(): ObservableArray<UserDetailModel> {
        return this._array;
    }

    constructor() {

        super();

        this._array = new ObservableArray([]);

        this._array.push({id:1,
                          username: "User 1", 
                          intro: 'intro 1',
                          blurb: 'blurb 1',
                          pic: "~/img/jr-sis.jpg",
                          keywords: []});
        
        this._array.push({id: 2,
                          username: "User 2",
                          intro: 'intro 2',
                          blurb: 'blurb 2', 
                          pic: "~/img/michelle.jpg",
                          keywords: []});

        this._array.push({id: 3,
                          username: "User 3", 
                          intro: 'intro 3',
                          blurb: 'blurb 3',
                          pic: "~/img/bang.jpg",
                          keywords: []});
    }
}

