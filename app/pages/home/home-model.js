"use strict";
var observable_1 = require("data/observable");
var observable_array_1 = require('data/observable-array');
var HomeViewModel = (function (_super) {
    __extends(HomeViewModel, _super);
    function HomeViewModel() {
        _super.call(this);
        this.busy = true;
        this.heading = 'Who\'s in the house?';
        this._array = new observable_array_1.ObservableArray([]);
        this._array.push({ id: 1,
            username: "User 1",
            intro: 'intro 1',
            blurb: 'blurb 1',
            pic: "~/img/jr-sis.jpg",
            keywords: [] });
        this._array.push({ id: 2,
            username: "User 2",
            intro: 'intro 2',
            blurb: 'blurb 2',
            pic: "~/img/michelle.jpg",
            keywords: [] });
        this._array.push({ id: 3,
            username: "User 3",
            intro: 'intro 3',
            blurb: 'blurb 3',
            pic: "~/img/bang.jpg",
            keywords: [] });
    }
    Object.defineProperty(HomeViewModel.prototype, "array", {
        get: function () {
            return this._array;
        },
        enumerable: true,
        configurable: true
    });
    return HomeViewModel;
}(observable_1.Observable));
exports.HomeViewModel = HomeViewModel;
//# sourceMappingURL=home-model.js.map