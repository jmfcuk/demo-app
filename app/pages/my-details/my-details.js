"use strict";
var BasePage_1 = require("../../shared/BasePage");
var observable_1 = require("data/observable");
var vm = new observable_1.Observable({ blackBackground: false });
var MyDetailsPage = (function (_super) {
    __extends(MyDetailsPage, _super);
    function MyDetailsPage() {
        _super.apply(this, arguments);
    }
    MyDetailsPage.prototype.mainContentLoaded = function (args) {
        var view = args.object;
        view.bindingContext = vm;
    };
    return MyDetailsPage;
}(BasePage_1.BasePage));
exports.MyDetailsPage = MyDetailsPage;
;
//# sourceMappingURL=my-details.js.map