import {BasePage} from "../../shared/BasePage";
import {Page} from "ui/page";
import {topmost} from "ui/frame";
import {Observable, EventData} from "data/observable";
import {View} from "ui/core/view";
import {ListView} from "ui/list-view";
import {UserDetailModel} from './user-detail-model';
import {ItemEventData} from "ui/list-view";

export class UserDetailPage extends BasePage{

    constructor() {
        super();
    }

    mainContentLoaded(args:EventData): void{

        let view = <View>args.object;

        view.bindingContext = new UserDetailModel();
    }

    searchSubmit(): void {
        //alert('searchSubmit');
    }

    listTap(args: ItemEventData): void {

        //let list: ListView = <ListView>super.getView('list');
        //let selected = list.getViewById((args.index + 1).toString());

        alert('list: ' + args.index); // + ' = ' + item.username);
    }
}

