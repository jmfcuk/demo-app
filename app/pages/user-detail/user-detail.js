"use strict";
var BasePage_1 = require("../../shared/BasePage");
var user_detail_model_1 = require('./user-detail-model');
var UserDetailPage = (function (_super) {
    __extends(UserDetailPage, _super);
    function UserDetailPage() {
        _super.call(this);
    }
    UserDetailPage.prototype.mainContentLoaded = function (args) {
        var view = args.object;
        view.bindingContext = new user_detail_model_1.UserDetailModel();
    };
    UserDetailPage.prototype.searchSubmit = function () {
        //alert('searchSubmit');
    };
    UserDetailPage.prototype.listTap = function (args) {
        //let list: ListView = <ListView>super.getView('list');
        //let selected = list.getViewById((args.index + 1).toString());
        alert('list: ' + args.index); // + ' = ' + item.username);
    };
    return UserDetailPage;
}(BasePage_1.BasePage));
exports.UserDetailPage = UserDetailPage;
//# sourceMappingURL=user-detail.js.map