
export class UserDetailModel {

    id: number;
    username: string; 
    intro: string;
    blurb: string;
    pic: string;
    keywords: Array<string>;
}


